import 'package:flutter/material.dart';

void main() {
  runApp(
    MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('I Am Rich'),
          centerTitle: true,
          backgroundColor: Colors.pink[800],
        ),
        backgroundColor: Colors.black,
        body: Center(
          child: Image(image: AssetImage('assets/gem.png')),
        ),
      ),
    ),
  );
}
